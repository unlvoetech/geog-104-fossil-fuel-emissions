'use strict';

angular.module('myApp', [
  'myApp.local-data',
  'myApp.home',
  'myApp.geog104-header',
  'myApp.geog104-footer',
	'ui.router',
  'range-slider-image-swap'
])

  .constant('config', {
    includes: {
      oeLogo: '//online.unlv.edu/sites/online.unlv.edu.oedrupal/libraries/base/Online_Education_500w_trimmed.png'
      // oeLogo: '//online.unlv.edu/sites/online.unlv.edu.oedrupal/libraries/base/Online_Education_500w_white_trimmed.png'
    }
  })

  .run([
    'config',
    '$rootScope',
    '$http',
    'Data',
    function (config, $rootScope, $http, Data) {

      $rootScope.config = config;

      Data.success(function(res) {
        $rootScope.appTitle = res.appTitle;
        $rootScope.coursePrefix = res.coursePrefix;
        $rootScope.courseNumber = res.courseNumber;
      });

      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      });

    }
  ])

  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$httpProvider',
    function($stateProvider, $urlRouterProvider, $httpProvider) {

      $stateProvider

        .state("Home", {
          url: "/",
          templateUrl: 'home/home.html',
          controller: 'HomeCtrl',
          controllerAs: 'vm'
        })

      $urlRouterProvider.otherwise(function($injector, $location) {
        var $state = $injector.get("$state");
        $state.go("Home");
      });

      $httpProvider.interceptors.push('customHttpInterceptor');

    }
  ])

  .service('customHttpInterceptor', [
    '$q',
    '$rootScope',
    function($q, $rootScope) {
      return {
        responseError: function(rejection) {
          console.error(rejection.statusText);
          return $q.reject(rejection);
        }
      }
    }
  ]);