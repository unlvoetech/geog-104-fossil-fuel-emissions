'use strict';

angular.module('myApp.home', [])

  .controller('HomeCtrl', [
    'Data',
    function(Data) {

      var vm = this;

      Data.then(function(res) {
        vm.data = res.data;
      });

    }
  ]);