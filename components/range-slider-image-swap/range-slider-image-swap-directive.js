'use strict';

var scripts = document.getElementsByTagName('script');
var rangeSliderImageSwapScriptPath = scripts[scripts.length-1].src;

angular.module('range-slider-image-swap.range-slider-image-swap-directive', [])

  .directive("rangeSliderImageSwap", [
    'Data',
    function(Data) {
      return {
        templateUrl: rangeSliderImageSwapScriptPath.replace('range-slider-image-swap-directive.js', 'range-slider-image-swap.html'),
        link: function(scope, element, attrs) {
          Data.then(function(res) {
            scope.data = res.data;
          });
          scope.positionChange = function(pos) {
            scope.data.rangeSlider.images.forEach(function(obj) {
              obj.active = false;
            });
            scope.data.rangeSlider.images[pos].active = true;
          }
        }
      }
    }
  ]);