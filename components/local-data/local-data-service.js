'use strict';

angular.module('myApp.local-data.local-data-service', [])

  .factory('Data', [
    '$http',
    function($http) {
      return $http.get('components/local-data/data.json');
    }
  ]);